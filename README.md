## About The Project

This is a test for the ZLI course M295.
I was assigned to create a RestAPI with NodeJS and ExpressJS. 
The API should be able to create, read, update and delete tasks.
I was given one day to complete the task.
This will get me a grade for the course.





### Built With

* [![Node][Node.js]][Node-url]





## Getting Started

This project was created with node.js and express.js.
So you need to have node.js installed on your machine.

### Prerequisites

* npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

1. Clone the repo
   ```sh
   git clone https://github.com/github_username/repo_name.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
3. Go to src folder
   ```sh
   cd src
   ```
4. Start the server
   ```sh
   npx nodemon app.js
   ```
5. Open your browser and go to
   ```sh
   localhost:3000
   ```
6. Try out the different endpoints
   ```sh
   localhost:3000/tasks
   localhost:3000/tasks/:id
   ```
   Scroll down to see all the endpoints.
   
7. Have fun!




## Usage

You can use this project to learn how to create a RestAPI with NodeJS and ExpressJS.
You can also use it as a template for your own projects.
You can create, read, update and delete tasks.
There is also a login function, but it is not completely implemented yet.






## Endpoints

- localhost:3000/tasks
  - GET
    - Get all tasks
  - POST
    - Create a new task

- localhost:3000/tasks/:id
    - GET
        - Get a task by id
    - PUT
        - Update a task by id
    - DELETE
        - Delete a task by id

- localhost:3000/login
    - POST
        - Login with username and password

- localhost:3000/verify
    - GET
        - Verify if the user is logged in

- localhost:3000/logout
    - DELETE
        - Logout the user




<!-- CONTACT -->
## Contact

Sananjayan - sananjayan.paramanantharajah@lernende.bbw.ch

Project Link: [https://gitlab.com/sa74/m295_practical_exam_sp](https://gitlab.com/sa74/m295_practical_exam_sp)

