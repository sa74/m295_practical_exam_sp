const swagger = require('swagger-autogen')();
const outputFile = './swagger_output.json';
const endpointFile = ['./ToDoListAPI.js'];

const doc = {
	info: {
		title: 'ToDo-List API',
		description: 'Documentation for the ToDo-List API',
		version: '1.0.0'
	}
},
  host: "localhost:3000", 
  basePath: "/",
  schemes: ["http"], 
  consumes: ["application/json"],
  produces: ["application/json"], 
  tasks:  [
    {
        name: "Tasks",
        description: "Tasks in ToDo-List"
    },
    {
        name: "Login",
        description: "Login in ToDo-List"
    }
  ],
  definitions: {
      Task: 
      {
        'title': 'Buy groceries',
        'created_date': '2023-05-01',
        'completed_date': '2023-05-02',
        'id': 1
      }
  }
};
  
  
  swagger(outputFile, endpointFile, doc);

