const express = require('express');
const session = require('express-session');
const app = express();
const port = 3000;


app.use(express.urlencoded({'extended': true}));
app.use(express.json());



app.use(session({
	secret: 'supersecret', 
	resave: false, 
	saveUninitialized: true,
	cookie: {}
}));

// generated with ChatGPT 
const tasks = [
	{
		'title': 'Buy groceries',
		'created_date': '2023-05-01',
		'completed_date': '2023-05-02',
		'id': 1
	},
	{
		'title': 'Pay bills',
		'created_date': '2023-05-03',
		'completed_date': '2023-05-04',
		'id': 2
	},
	{
		'title': 'Go for a run',
		'created_date': '2023-05-05',
		'completed_date': null,
		'id': 3
	},
	{
		'title': 'Finish work presentation',
		'created_date': '2023-05-06',
		'completed_date': null,
		'id': 4
	},
	{
		'title': 'Call mom',
		'created_date': '2023-05-07',
		'completed_date': '2023-05-07',
		'id': 5
	}
];

app.get('/tasks', (request, response) => {
// #swagger.tags = ["Tasks"]
// #swagger.description = "Endpoint to get a list with every task"
	response.set('Content-Type', 'application/json');
	response.status(200).json(tasks);
});


let nextTaskId = tasks.length + 1;

app.post('/tasks', (request, response) => {
// #swagger.tags = ["Tasks"]
// #swagger.description = "Endpoint to create a new task"
	const newTask = request.body;
	response.set('Content-Type', 'application/json');
	if (!request.body.title){
		console.log('Cant create task without title ');
		return response.status(406).json({ message : 'no value for title' });
	}
	newTask.id = nextTaskId++;
	tasks.push(newTask);
	console.log(newTask);
	response.status(201).json(tasks);
});

app.get('/tasks/:id', (request, response) => {
// #swagger.tags = ["Tasks"]
// #swagger.description = "Endpoint to get a specific task out of the list by the id"
	const id = request.params.id;
	response.set('Content-Type', 'application/json');
	const task = tasks.find((task) => task.id == id);
	if (!task){
		console.log('couldnt find task');
		response.sendStatus(404).json({ message: 'No Task with this ID' });
	}
	response.status(200).json(task);
}
);

app.put('/tasks/:id', (request, response) => {
// #swagger.tags = ["Tasks"]
// #swagger.description = "Endpoint to update a specific task in the array"
	const id = request.params.id;
	const task = tasks.findIndex((task) => task.id == id);
	response.set('Content-Type', 'application/json');
	if (!task){
		console.log('Does not exist');
		response.status(404).json({ message : 'not found'});
	}
        
	const changedTask = request.body;
	tasks[task] = {
		...tasks[task],
		...changedTask
	};
	console.log(changedTask);
	response.status(200).json(tasks[task]);
});

app.delete('/tasks/:id', (request, response) => {
// #swagger.tags = ["Tasks"]
// #swagger.description = "Endpoint to delete a specific task"
	const id = request.params.id;
	const task = tasks.findIndex(task => task.id === id);
	response.set('Content-Type', 'application/json');
	if (!task){
		console.log('Does not exist');
		response.status(404).json({message : 'not found'});
	}
	tasks.splice(task, 1);
	console.log(task);
	response.sendStatus(200);
});


const password = 'm295';

app.post('/login', (request, response) => {
// #swagger.tags = ["Login"]
// #swagger.description = "Endpoint to login with email and password"

	const email = request.body.email;
	response.set('Content-Type', 'application/json');

    
	const emailRequirment = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    
	if(request.body.password !== password || !emailRequirment.test(email)) {
		console.log('login failed');
		return response.status(401).json({message: 'Wrong Password or Email'});
	}
	request.session.password = request.body.password;
	response.status(200).json({message: 'You\'re logged in!'});

});

app.get('/verify', (request, response) => {
// #swagger.tags = ["Login"]
// #swagger.description = "Endpoint to verify the login"
	response.set('Content-Type', 'application/json');

	if(!request.session.password) {
		console.log('couldnt verify');
		return response.status(401).json({ message : 'failed to verify'} );
	}
	response.status(200).json({message : 'verified'});
});

app.delete('/logout', (request, response) => {
// #swagger.tags = ["Login"]
// #swagger.description = "Endpoint to logout again"
	request.session.destroy();
	response.sendStatus(204);
});

app.get('/*', (request, response) => {
	response.status(404).json({message : 'Not Available'});
});
  

app.listen(port, () => {
	console.log('Currently running on port ' + port);
});